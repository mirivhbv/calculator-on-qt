#ifndef CALCULATEPROCESSTHREAD_H
#define CALCULATEPROCESSTHREAD_H

#include <qthread.h>
#include <QDebug>
#include "queuerequest.h"

class CalculateProcessThread :
    public QThread
{
    Q_OBJECT // Don't forget this MACRO to let this class emit SIGNALS

public:
    CalculateProcessThread();
    ~CalculateProcessThread();
    void run();

    double compute( int Type, double OperandA, double OperandB);       //функция вычислений
signals:
    void processDone(double); // SIGNAL to show that some process have been done

private:
    std::mutex m1;
};

#endif // CALCULATEPROCESSTHREAD_H
