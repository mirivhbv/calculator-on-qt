#ifndef REQUEST_H
#define REQUEST_H


class Request
{
public:
    Request();
    Request(int type, double operandA, double operandB, int delayMS = 0);

    int m_type;
    double m_operandA;
    double m_operandB;

    int m_delayMS; // delay millisecond, TODO: change it to unsigned long
};

#endif // REQUEST_H
