#include "request.h"

Request::Request()
{
    this->m_type = -1;
    this->m_operandA = -1;
    this->m_operandB = -1;
    this->m_delayMS = 0;
}

Request::Request(int type, double operandA, double operandB, int delayMS)
{
    this->m_type = type;
    this->m_operandA = operandA;
    this->m_operandB = operandB;
    this->m_delayMS = delayMS;
}
