#ifndef QUEUEREQUEST_H
#define QUEUEREQUEST_H

#include <queue>
#include <mutex>
#include "request.h"

class QueueRequest
{
public:
    static std::queue<Request>* requests;
    static std::mutex* m1;
private:
    QueueRequest();
};

#endif // QUEUEREQUEST_H
